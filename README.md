## Sample Nginx Configuration Files with SSL for local development

### Clone the project

```
git clone https://gitlab.com/techantidote/nginx.git
cd nginx
````

You should be able to see default.conf and a static html page.

```
find .
```

```
.
./html
./html/index.html
./default.conf
./README.md
```

Modify server_name in default.conf with FQDN:

    server_name  webserver.homelab webserver localhost;


### Run nginx container


```
sudo docker run --name docker-nginx --net=host -v ${PWD}/html:/usr/share/nginx/html -v ${PWD}/default.conf:/etc/nginx/conf.d/default.conf -d nginx

```
*Alternative:* Run using docker networking by exposing ports:

```
sudo docker run --name docker-nginx -p 443:443 -p 80:80 -v ${PWD}/html:/usr/share/nginx/html -v ${PWD}/default.conf:/etc/nginx/conf.d/default.conf -d nginx
```

## SSL Keys + Certs

=> Generate your private key and copy to location /etc/ssl/webserver.key inside nginx container.

=> Copy the signed certificate to location /etc/ssl/webserver.crt inside the nginx container.

=> If you need to modify any of these values, you can update the following entries in your config file:

    ssl_certificate /etc/ssl/webserver.crt;
    ssl_certificate_key /etc/ssl/webserver.key;


### Contact Information

-Would you like to contribute to this project? Or if you have any feedback, you can reach out to me in twitter.

https://twitter.com/techantidote


### Other Links

https://techantidote.com
https://gitlab.com/techantidote


